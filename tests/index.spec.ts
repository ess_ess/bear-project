// ps -> Iuse fedora for personal work and playwright wont work in fedora
// See https://github.com/microsoft/playwright/issues/14736#issuecomment-1160912303
import { test, expect, type Page } from "@playwright/test";

test.beforeEach(async ({ page }) => {
  // todo: Add route/url to env variable
  await page.goto("http://localhost:3000/");
});

test.describe.skip("Location Table", () => {
  test("should render location table UI", async ({ page }) => {
    const table = page.getByTestId("BearDataGrid");
    const searchDropdown = page.getByTestId("SearchGroups");
    const searchInput = page.getByTestId("Search");
  });

  test("should add searchGroup queryParam on searchDropdown", async ({
    page,
  }) => {});

  test("should add search URL queryParam on searchInput", async ({
    page,
  }) => {});
});

test.describe.skip("DataGrid", () => {
  test("should change starred state on clicking star button", async ({
    page,
  }) => {
    // to implement
  });
});
