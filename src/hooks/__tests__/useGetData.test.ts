import useGetData, { depsToQueryParams } from "../useGetData";

describe.skip("useGetData", () => {
  // test initialize
  it("should have loading to true and results to null on intialize", () => {
    // useGetData();
  });
  // use https://testing-library.com/docs/react-testing-library/api/#renderhook
  // mock API call, see if the mock API is called with correct params
  // see results
  it("should fetch results from backend", () => {});
  // mock API to fail, and see if loading is still false
  it("should set loading to false even when API errors", () => {});
});

describe.skip("depsToQueryParams", () => {
  it("should handle case when no dependencies are provided", () => {});
  it("should handle case when only one dependency is provided", () => {});
  it("should handle case when multiple  dependencies are provided", () => {});
});
