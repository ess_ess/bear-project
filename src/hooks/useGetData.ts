import { useEffect, useState } from "react";
import { Location, LocationsPathParams, SEARCH_IDS } from "../types";

type Deps = {
  page?: string;
  search?: string;
  searchGroup?: SEARCH_IDS;
};

// page: string;
// location_name: string;
// robot_id: string;
// is_starred: string;
export function depsToQueryParams(deps: Deps): URLSearchParams {
  const { page, search, searchGroup } = deps;
  // Hmm.. this convevrsion here is a bit unnecessary, IMO
  // If I have time, I will comeback and simplify this whole
  // query param business. For now, this is over complicated without
  // any reason
  const params = new URLSearchParams();
  page && params.append("page", page);
  search && params.append("location_name", search);
  (searchGroup === "ALL" || searchGroup === "STARRED") &&
    params.append("is_starred", String(searchGroup === "STARRED"));
  return params;
}

type LocationsResult = {
  total_count: number;
  locations: Location[];
};

export default function useGetData(deps: Deps): {
  result?: LocationsResult;
  loading: boolean;
} {
  const { page, search, searchGroup } = deps;
  const [loading, setLoading] = useState(true);

  // maybe I would use react-query or restructure the data
  const [result, setResult] = useState<LocationsResult>();
  useEffect(() => {
    setLoading(true);
    // create params
    const params = depsToQueryParams(deps);
    fetch(`/locations?${params}`)
      .then((r) => {
        r.json().then((l) => {
          setResult(l);
        });
      })
      .catch((e) => {
        // to do: maybe show a alert bar
        console.warn(e);
        setResult({
          total_count: 0,
          locations: [],
        });
      })
      .finally(() => {
        setLoading(false);
      });
  }, [page, search, searchGroup]);
  return { result, loading };
}
