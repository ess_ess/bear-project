import "./App.css";

import { RouterProvider } from "react-router-dom";
import { createBrowserRouter } from "react-router-dom";

import { ErrorPage } from "./components/Error";
import Layout from "./components/Layout";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    errorElement: <ErrorPage />,
  },
]);

function App() {
  return (
    <div className="app__wrap">
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
