import { faker } from "@faker-js/faker";
import { factory, oneOf, primaryKey } from "@mswjs/data";

export const db = factory({
  // Create a "location" model,
  location: {
    id: primaryKey(faker.string.uuid),
    name: faker.person.fullName,
    robot: oneOf("robot"),
    starred: faker.datatype.boolean,
  },
  robot: {
    id: primaryKey(faker.string.uuid),
    is_online: faker.datatype.boolean,
  },
});

export type DbModal = typeof db;
