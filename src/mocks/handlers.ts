import { DefaultBodyType, rest } from "msw";

import {
  LocationsPathParams,
  LocationsResult,
  StarredPutReqBody,
  StarredPutResultBody,
} from "../types";
import { DbModal, db } from "./db";
import { BooleanQuery, StringQuery } from "@mswjs/data/lib/query/queryTypes";
import { PAGE_SIZE, TOTAL_SIZE } from "../consts";

function seedData({ db }: { db: DbModal }) {
  new Array(TOTAL_SIZE).fill(0).forEach((i) => {
    const robot = db.robot.create();
    db.location.create({
      robot,
    });
  });
}

const handlerFactory = ({ db }: { db: DbModal }) => {
  seedData({ db });
  return [
    rest.get<DefaultBodyType, LocationsPathParams, LocationsResult>(
      "/locations",
      (req, res, ctx) => {
        const page = req.url.searchParams.get("page");
        const pageNum = Number(page);
        const locationName = req.url.searchParams.get("location_name");
        const isStarred = req.url.searchParams.get("is_starred");
        // Please implement filtering feature here

        const nameQuery: Partial<StringQuery> = {};
        const isStarredQuery: Partial<BooleanQuery> = {};
        locationName && (nameQuery.contains = locationName);
        isStarred !== null && (isStarredQuery.equals = Boolean(isStarred));

        const results = db.location.findMany({
          where: {
            name: nameQuery,
            starred: isStarredQuery,
          },
          take: PAGE_SIZE,
          skip: pageNum * PAGE_SIZE || 0,
        });
        const result: LocationsResult = {
          // ideally should be count of the query
          total_count: db.location.count(),
          // @ts-expect-error
          locations: results,
        };
        return res(ctx.status(200), ctx.json(result));
      }
    ),

    rest.get("/starred_location_ids", (req, res, ctx) => {
      const location_ids = JSON.parse(
        sessionStorage.getItem("starred_location_ids") || "[]"
      );

      return res(
        ctx.status(200),
        ctx.json({
          location_ids,
        })
      );
    }),

    rest.put<StarredPutReqBody, any, StarredPutResultBody>(
      "/starred_location_ids",
      (req, res, ctx) => {
        const { id, starred } = req.body;
        const updatedLocation = db.location.update({
          // Query for the entity to modify.
          where: {
            id: {
              equals: id,
            },
          },
          data: {
            // Specify the exact next value.
            starred,
          },
        });
        console.warn("backed changed to", {
          updatedLocation,
          starred: updatedLocation?.starred,
        });
        return res(
          // need to google and see correct status codes :D
          ctx.status(200),
          ctx.json({
            id: updatedLocation?.id || "",
            starred: updatedLocation?.starred || false,
          })
        );
      }
    ),
  ];
};

export const handlers = handlerFactory({ db });
