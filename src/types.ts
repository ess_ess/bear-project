export type SEARCH_IDS = "ALL" | "STARRED";

export type Robot = {
  id: string;
  is_online: boolean;
};

export type Location = {
  id: number;
  name: string;
  robot: Robot;
};

export interface LocationsPathParams {
  page: string;
  location_name: string;
  robot_id: string;
  is_starred: string;
}

export type StarredPutReqBody = { id: string; starred: boolean };
export type StarredPutResultBody = { id: string; starred: boolean };

export type LocationsResult = {
  total_count: number;
  locations: Location[];
};
