import { renderWithRouter } from "../../../test-utils";
import Layout from "../Layout";

describe.skip("Layout", () => {
  it("should render layout", () => {
    const { asFragment } = renderWithRouter(<Layout />);
    expect(asFragment()).toMatchSnapshot();
  });
});
