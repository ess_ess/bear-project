import React from "react";
import "@testing-library/jest-dom";

import { renderWithRouter } from "../../../test-utils";

import Body from "../Body";

// this kind of snapshot test are good to see layouts
test("renders layout of Body", () => {
  const { asFragment } = renderWithRouter(<Body />);
  expect(asFragment()).toMatchSnapshot();
});
