import React from "react";
import RefreshIcon from "@mui/icons-material/Refresh";

export default function Loadig() {
  return (
    <div
      style={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <RefreshIcon />
    </div>
  );
}
