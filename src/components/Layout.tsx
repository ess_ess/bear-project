import Body from "./Body";
import Title from "./Title";

export default function Layout() {
  return (
    <div className="layout__wrap">
      <div className="layout__title">
        <Title />
      </div>
      <div className="layout__body">
        <Body />
      </div>
    </div>
  );
}
