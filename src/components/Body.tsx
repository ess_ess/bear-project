import Search from "./Search";
import SearchGroups from "./SearchGroups";
import BearDataGrid from "./DataGrid";

//  ideally, I would organize components to more heirarchies
export default function Body() {
  return (
    <div className="table__wrap">
      <div className="table__title">
        <div className="table__title--left">
          <SearchGroups />
        </div>
        <div className="table__title--right">
          <Search />
        </div>
      </div>
      <div className="table__body">
        <BearDataGrid />
      </div>
    </div>
  );
}
