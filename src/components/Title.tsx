import { Typography } from "@mui/material";

export default function Title() {
  return (
    <div className="title__wrap">
      <Typography variant="h4">Your fleet</Typography>
    </div>
  );
}
