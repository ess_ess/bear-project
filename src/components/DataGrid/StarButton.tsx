import StarIcon from "@mui/icons-material/Star";
import { IconButton } from "@mui/material";
import { useCallback, useEffect, useState } from "react";

import { StarredPutResultBody } from "../../types";

export default function StarButton({
  checked,
  id,
}: {
  checked: boolean;
  id: string;
}) {
  // maybe move hooks out to custom hooks for testing ease
  const [starred, setStarred] = useState(checked);
  useEffect(() => {
    setStarred(checked);
  }, [checked]);
  const onClick = useCallback(() => {
    // for instant feedback
    setStarred(!starred);
    fetch("/starred_location_ids", {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        id,
        starred: !starred,
      }),
    }).then((r) => {
      r.json().then((result: StarredPutResultBody) => {
        // set correct value later
        setStarred(result.starred);
      });
    });
  }, [id, starred]);
  return (
    <IconButton onClick={onClick}>
      <StarIcon color={starred ? "primary" : "disabled"} />
    </IconButton>
  );
}
