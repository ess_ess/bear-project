import BearDataGrid from "..";
import { renderWithRouter } from "../../../../test-utils";

// should mock useGetData
describe.skip("datagrid", () => {
  it("should render data grid", () => {
    const { container } = renderWithRouter(BearDataGrid, {
      route: `/`,
    });
    // check layout, see compoenents are up using `data-testid`
    // expect(container.querySelector('aria-rowindex="1"')) // to find coloumn
    // see if coloumn headers are rendered properly
  });
  it("should show loading screen when result is not ready", () => {});
  it("should render custom icons for stars", () => {});
  it("should render pagination on footer", () => {});
  it("should change query param: page on clicking pagination", () => {});
});
