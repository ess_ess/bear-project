describe.skip("StarButton", () => {
  it("should render star icon", () => {});
  // donot need to check API response here
  it("should make API call to change starred state", () => {});
  it("should set active color when starred is true", () => {});
  it("should set inactive color when starred is false", () => {});
});
