import Box from "@mui/material/Box";
import {
  DataGrid,
  GridColDef,
  GridRenderCellParams,
  GridValueGetterParams,
} from "@mui/x-data-grid";
import Pagination from "@mui/material/Pagination";
import PaginationItem from "@mui/material/PaginationItem";
import { Link, useSearchParams } from "react-router-dom";
import useGetData from "../../hooks/useGetData";
import { PAGE_SIZE } from "../../consts";
import ReplayIcon from "@mui/icons-material/Replay";
import StarButton from "./StarButton";
import Loading from "../Loading";

function ResetGridCell() {
  return <ReplayIcon />;
}

const columns: GridColDef[] = [
  {
    field: "star",
    // not sure what action is expected
    renderHeader: () => {
      return <ResetGridCell />;
    },
    renderCell: (data: GridRenderCellParams) => {
      const starred: boolean = Boolean(data.row?.starred);
      const id: string = data.row?.id;
      return <StarButton checked={starred} id={id} />;
    },
  },
  { field: "id", headerName: "ID", width: 90 },
  {
    field: "name",
    headerName: "Restaurant name",
    width: 350,
  },
  {
    field: "robot.id",
    valueGetter: (r: GridValueGetterParams) => {
      return r.row.robot.id;
    },
    headerName: "Robot",
    width: 450,
    editable: false,
  },
];

export default function BearDataGrid() {
  const [query] = useSearchParams();
  const page = query.get("page") || "0";
  const search = query.get("search");
  const searchGroup = query.get("searchGroup");
  // @ts-expect-error string|null -> string.undefined
  const { result, loading } = useGetData({ page, search, searchGroup });
  if (result?.locations === undefined) {
    return <Loading />;
  }
  return (
    <Box data-testid="BearDataGrid" sx={{ height: 600, width: "100%" }}>
      <DataGrid
        rows={result.locations}
        loading={loading}
        columns={columns}
        initialState={{
          pagination: {
            paginationModel: {
              pageSize: PAGE_SIZE,
              page: 0,
            },
          },
        }}
        // not sure what UX is expected
        checkboxSelection
        disableRowSelectionOnClick
        hideFooterPagination
      />
      <div className="pagination__wrap">
        <Pagination
          count={Math.floor(result.total_count / PAGE_SIZE)}
          renderItem={(item) => (
            <PaginationItem
              component={Link}
              to={`${item.page === 1 ? "" : `?page=${item.page}`}`}
              {...item}
            />
          )}
        />
      </div>
    </Box>
  );
}
