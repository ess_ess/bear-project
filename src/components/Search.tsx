import { TextField } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import { useDebouncedCallback } from "use-debounce";
import { useSearchParams } from "react-router-dom";

export default function Search() {
  // might be better to move to custom hook :)
  // same pattern as useGetData
  // good for testing
  const [search, setSearch] = useSearchParams();
  const debouncedOnChange: React.ChangeEventHandler<HTMLInputElement> =
    useDebouncedCallback((e) => {
      // to do: donot overwrite prev. query
      setSearch({ search: e?.target?.value });
    }, 500);

  const defaultValue = search.get("search") || "";

  return (
    <div className="search__wrap" data-testid="Search">
      <TextField
        id="search"
        label="Search robot or location"
        variant="outlined"
        onChange={debouncedOnChange}
        defaultValue={defaultValue}
      />
      <div className="search__icon">
        <SearchIcon />
      </div>
    </div>
  );
}
