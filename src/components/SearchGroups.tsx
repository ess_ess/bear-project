import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { useSearchParams } from "react-router-dom";

import { SEARCH_IDS } from "../types";

type SEARCH_LABEL = {
  key: SEARCH_IDS;
  value: string;
};

const searches: SEARCH_LABEL[] = [
  {
    key: "ALL",
    value: "All locations",
  },
  {
    key: "STARRED",
    value: "starred",
  },
];

export default function SearchGroups() {
  const [search, setSearch] = useSearchParams();

  const handleChange = (event: SelectChangeEvent) => {
    setSearch({ searchGroup: event.target.value });
  };

  const defaultValue = search.get("searchGroup") || "";

  return (
    <Box sx={{ minWidth: 360 }} data-testid="SearchGroups">
      <FormControl fullWidth>
        <InputLabel id="search-groups-label">Search Locations</InputLabel>
        <Select
          labelId="search-groups-label"
          id="search-groups"
          label="All locations"
          onChange={handleChange}
          defaultValue={defaultValue}
        >
          {searches.map((s) => (
            <MenuItem key={s.key} value={s.key}>
              {s.value}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  );
}
