# FE interview take-home project

We are going to create a simple fleet management dashboard

Work done so far:

- Basic layout
- Atomic components
- Mock API response
- Url based data flow
- End to end tests -> Playwright tests setup
- Unit tests setup -> React resting library

Todo:

- Cleanup routing
  - Now only one query param is added at a time
- Some DB cleanups:
  - Get correct total results on filter(couldnt find on MSW/data's API docs)
  - etc...
- Cleanup some Type definitions - especially for routing
  - I tried to produce max product on time, this was a compromise
- Add tests:
  - I have outlined my testing strategy / thought process for selecting
    which test cases to run
  - IMO, adding maximum possible tests in not viable for an assignment ~
  - playwright wont work on my personal laptop(fedora)
    - I can potentially run it on CI / docker to test, but not worth the effort
      for this assignment

## How it works ->

We use Mui DataGrid to create the table
Also use some MUI components to add other UI components
Data is mocked using mock service worker: MSW
MSW intercepts the API calls and send back mocked response

- Source code is in `\src`
  - Components in `./src/components`
  - Custom hooks in `./src/components/hooks`
- API is mocked with [MSW](https://github.com/mswjs/msw)
  - See `./src/mocks`
  - See subdirectories
    - `browser` for setup of service worker
    - `db` for mock Database models
    - `handlers` Routes handling by service worker
- End to end tests are in `./tests`
- Unit tests are in `__tests__` of respective component/function

## Requirements

```
nodejs 18
npm 9.5
```

## install

```
npm i
```

## Developement

```
npm start
```

## Test

```
npm t
```

## End to end tests

```
npx playwright test
```
